# frozen_string_literal: true

Order.create!(
  ingredients: %w[avocado carrot lettuce meat_ball sauce tofu tomato],
  price: 1840,
  description: 'Sandwich, Avocado, Carrot, Lettuce, Meat ball, Sauce, Tofu, Tomato'
)

Order.create!(
  ingredients: %w[avocado tomato],
  price: 1260,
  description: 'Sandwich, Avocado, Tomato'
)

Order.create!(
  ingredients: %w[avocado carrot sauce meat_ball],
  price: 1580,
  description: 'Sandwich, Avocado, Carrot, Sauce, Meat ball'
)
