# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.string :ingredients, array: true, null: false
      t.integer :price, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
