# frozen_string_literal: true

class CarrotDecorator < IngredientDecorator
  def price
    80 + super
  end

  def description
    "#{super}, Carrot"
  end
end
