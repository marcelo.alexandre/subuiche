# frozen_string_literal: true

class AvocadoDecorator < IngredientDecorator
  def price
    200 + super
  end

  def description
    "#{super}, Avocado"
  end
end
