# frozen_string_literal: true

class LettuceDecorator < IngredientDecorator
  def price
    50 + super
  end

  def description
    "#{super}, Lettuce"
  end
end
