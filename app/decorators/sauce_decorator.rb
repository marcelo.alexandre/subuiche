# frozen_string_literal: true

class SauceDecorator < IngredientDecorator
  def price
    100 + super
  end

  def description
    "#{super}, Sauce"
  end
end
