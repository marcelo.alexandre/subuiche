# frozen_string_literal: true

class TomatoDecorator < IngredientDecorator
  def price
    60 + super
  end

  def description
    "#{super}, Tomato"
  end
end
