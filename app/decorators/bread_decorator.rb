# frozen_string_literal: true

class BreadDecorator < IngredientDecorator
  def price
    1000 + super
  end

  def description
    'Sandwich'
  end
end
