# frozen_string_literal: true

class IngredientDecorator
  def initialize(object = nil)
    @object = object
  end

  def price
    object&.price || 0
  end

  def description
    object&.description
  end

  private

  attr_reader :object
end
