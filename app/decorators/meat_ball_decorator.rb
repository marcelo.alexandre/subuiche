# frozen_string_literal: true

class MeatBallDecorator < IngredientDecorator
  def price
    200 + super
  end

  def description
    "#{super}, Meat ball"
  end
end
