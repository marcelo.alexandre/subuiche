# frozen_string_literal: true

class TofuDecorator < IngredientDecorator
  def price
    150 + super
  end

  def description
    "#{super}, Tofu"
  end
end
