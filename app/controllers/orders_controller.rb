# frozen_string_literal: true

class OrdersController < ApplicationController
  def index
    orders = Order.all

    render(json: orders.map { |order| OrderSerializer.new(order).as_json })
  end

  def create
    result = PlaceOrder.call(ingredients: create_order_params[:ingredients])

    if result.failure?
      return render(
        status: :unprocessable_entity,
        json: ValidationErrorSerializer.new(result.data[:errors]).as_json
      )
    end

    render(status: :created, json: OrderSerializer.new(result.data[:order]).as_json)
  end

  private

  def create_order_params
    params.require(:order).permit(ingredients: [])
  end
end
