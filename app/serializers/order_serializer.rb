# frozen_string_literal: true

class OrderSerializer
  def initialize(order)
    @order = order
  end

  def as_json
    {
      'id' => order.id,
      'ingredients' => order.ingredients,
      'price' => order.price,
      'description' => order.description,
      'placedAt' => order.created_at
    }
  end

  private

  attr_reader :order
end
