# frozen_string_literal: true

class PlaceOrder < ApplicationService
  def initialize(ingredients:)
    @ingredients = ingredients || []
  end

  def call
    sandwich = build_sandwich

    order = Order.new(ingredients:, price: sandwich.price, description: sandwich.description)

    return FailureResult.new(data: { errors: order.errors.full_messages }) if order.invalid?

    order.save!

    SuccessResult.new(data: { order: })
  end

  private

  attr_reader :ingredients

  INGREDIENT_DECORATORS = {
    'avocado' => AvocadoDecorator,
    'carrot' => CarrotDecorator,
    'lettuce' => LettuceDecorator,
    'meat_ball' => MeatBallDecorator,
    'sauce' => SauceDecorator,
    'tofu' => TofuDecorator,
    'tomato' => TomatoDecorator
  }.freeze
  private_constant :INGREDIENT_DECORATORS

  def build_sandwich
    INGREDIENT_DECORATORS
      .select { |ingredient, _ingredient_class| ingredients.include?(ingredient) }
      .values
      .reduce(BreadDecorator.new) { |ingredient, ingredient_class| ingredient_class.new(ingredient) }
  end
end
