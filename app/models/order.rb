# frozen_string_literal: true

class Order < ApplicationRecord
  serialize :ingredients, Array

  validates :ingredients, presence: true
  validates :ingredients, inclusion: { in: %w[avocado carrot lettuce meat_ball sauce tofu tomato] }
  validates :price, presence: true
end
