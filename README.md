# Subuíche

This is a fictitious sandwich maker application that allows to place orders of sandwiches selecting ingredients. It was developed to present problems that can be solved by applying design patterns.

## Versions

- V1: https://gitlab.com/marcelo.alexandre/subuiche/-/tree/1ee53b03bb72c96c771e2d0179e66b9a5bbc9318
- V2 (Refactor using Serializer): https://gitlab.com/marcelo.alexandre/subuiche/-/tree/15af6681b99b32f8c5b4fc030eb163323fceca8f
- V3 (Refactor using Service Object): https://gitlab.com/marcelo.alexandre/subuiche/-/tree/566dc76ffbbdc0c78460ae7812f2a24bdaa9ccb8
- V4 (Refactor using Decorator): https://gitlab.com/marcelo.alexandre/subuiche/-/tree/3a1cfe14bbf0fb80c3949795d94ece469da52955

## System dependencies

- Ruby 3.2.1
- SQLite
- Docker

## Setup

```bash
# Install dependencies
$ dbin/bundle

# Create database and seed data
$ dbin/rails db:setup
```

## Running tests

```bash
$ dbin/rspec
```

## Running linter

```bash
$ dbin/rubocop
```

## Start the application

```bash
$ dbin/rails s
```

## API Docs

- http://localhost:3000/api-docs
