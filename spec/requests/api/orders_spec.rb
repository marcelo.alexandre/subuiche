require 'swagger_helper'

RSpec.describe 'Orders', type: :request do
  path '/orders' do
    get 'Retrieves all orders' do
      tags 'Orders'
      consumes 'application/json'

      response '200', 'orders found' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer },
              ingredients: {
                type: :array,
                items: {
                  type: :string,
                  enum: %w[avocado carrot lettuce meat_ball sauce tofu tomato]
                }
              },
              price: { type: :integer },
              description: { type: :string },
              placedAt: { type: :string, format: :date_time }
            }
          }

          run_test!
      end
    end

    post 'Places a order' do
      tags 'Orders'
      consumes 'application/json'
      parameter name: :order, in: :body, schema: {
        type: :object,
        properties: {
          ingredients: {
            type: :array,
            items: {
              type: :string,
              enum: %w[avocado carrot lettuce meat_ball sauce tofu tomato]
            }
          },
        },
        required: %w[ingredients],
        example: {
          ingredients: %w[avocado carrot lettuce meat_ball sauce tofu tomato]
        }
      }

      response '201', 'order placed' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            ingredients: {
              type: :array,
              items: {
                type: :string,
                enum: %w[avocado carrot lettuce meat_ball sauce tofu tomato]
              }
            },
            price: { type: :integer },
            description: { type: :string },
            placedAt: { type: :string, format: :date_time }
          }

        let(:order) { { ingredients: %w[avocado carrot lettuce meat_ball sauce tofu tomato] } }

        run_test!
      end

      response '422', 'unprocessable entity' do
        schema type: :object,
          properties: {
            error: {
              type: :object,
              properties: {
                message: { type: :string },
                details: { type: :array, items: { type: :string } }
              }
            }
          }

        let(:order) { { ingredients: [] } }

        run_test!
      end
    end
  end
end
