# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Orders' do
  describe 'GET /orders' do
    context 'when orders exist' do
      let!(:orders) { create_list(:order, 3) }

      it 'responds with 200 (:ok)' do
        get('/orders')

        expect(response).to have_http_status(:ok)
      end

      it 'returns orders in the response' do
        get('/orders')

        expect(JSON.parse(response.body))
          .to eq(
            orders.map do |order|
              {
                'id' => order.id,
                'ingredients' => order.ingredients,
                'price' => order.price,
                'description' => order.description,
                'placedAt' => order.created_at.as_json
              }
            end
          )
      end
    end

    context 'when orders does not exist' do
      it 'responds with 200 (:ok)' do
        get('/orders')

        expect(response).to have_http_status(:ok)
      end

      it 'returns an empty array in the response' do
        get('/orders')

        expect(JSON.parse(response.body)).to eq([])
      end
    end
  end
end
