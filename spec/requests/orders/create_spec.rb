# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Orders' do
  describe 'POST /orders' do
    let(:params) do
      {
        order: {
          ingredients: %w[avocado carrot lettuce meat_ball sauce tofu tomato]
        }
      }
    end

    context 'when params are valid' do
      it 'responds with 201 (:created)' do
        post('/orders', params:)

        expect(response).to have_http_status(:created)
      end

      it 'places a new order' do
        expect { post('/orders', params:) }
          .to change(Order, :count).by(1)
          .and change { Order.last&.ingredients }.to(%w[avocado carrot lettuce meat_ball sauce tofu tomato])
          .and change { Order.last&.price }.to(1840)
          .and change {
                 Order.last&.description
               }.to('Sandwich, Avocado, Carrot, Lettuce, Meat ball, Sauce, Tofu, Tomato')
      end

      it 'returns the new placed order in the response' do
        post('/orders', params:)

        new_order = Order.last
        expect(JSON.parse(response.body)).to eq(
          'id' => new_order.id,
          'ingredients' => new_order.ingredients,
          'price' => new_order.price,
          'description' => new_order.description,
          'placedAt' => new_order.created_at.as_json
        )
      end

      context 'when ingredients contain "avocado" and "tomato"' do
        let(:params) do
          {
            order: {
              ingredients: %w[avocado tomato]
            }
          }
        end

        it 'places a new order with the correct price' do
          expect { post('/orders', params:) }
            .to change(Order, :count).by(1)
            .and change { Order.last&.ingredients }.to(%w[avocado tomato])
            .and change { Order.last&.price }.to(1260)
            .and change { Order.last&.description }.to('Sandwich, Avocado, Tomato')
        end
      end

      context 'when ingredients contain "avocado", "carrot", "sauce", and "meat_ball"' do
        let(:params) do
          {
            order: {
              ingredients: %w[avocado carrot sauce meat_ball]
            }
          }
        end

        it 'places a new order with the correct price' do
          expect { post('/orders', params:) }
            .to change(Order, :count).by(1)
            .and change { Order.last&.ingredients }.to(%w[avocado carrot sauce meat_ball])
            .and change { Order.last&.price }.to(1580)
            .and change { Order.last&.description }.to('Sandwich, Avocado, Carrot, Meat ball, Sauce')
        end
      end
    end

    context 'when ingredients are invalid' do
      let(:params) do
        {
          order: {
            ingredients: nil
          }
        }
      end

      it 'responds with 422 (:unprocessable_entity)' do
        post('/orders', params:)

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'does not place a new order' do
        expect { post('/orders', params:) }.not_to change(Order, :count)
      end

      it 'returns a error details in the response' do
        post('/orders', params:)

        expect(JSON.parse(response.body)).to include(
          'error' => include('message' => 'Validation error', 'details' => ["Ingredients can't be blank"])
        )
      end
    end
  end
end
