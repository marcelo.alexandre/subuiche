# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    ingredients { %w[avocado carrot lettuce meat_ball sauce tofu tomato] }
    price { 1840 }
    description { 'Sandwich, Avocado, Carrot, Lettuce, Meat ball, Sauce, Tofu, Tomato' }
  end
end
